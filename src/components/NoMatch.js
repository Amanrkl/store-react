import React from 'react';

const NoMatch = () => {
    return ( 
        <div className='error'>No Page Found</div>
     );
}
 
export default NoMatch;