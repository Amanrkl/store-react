import React, { Component } from 'react';
import '../assets/style/product.css';
import axios from 'axios';

import Loader from './Loader';

class CartProducts extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        };

        this.state = {
            product: {},
            status: this.API_STATES.LOADING,
            errorMessage: "",
        };
    }

    fetchData = (url) => {
        this.setState({
            status: this.API_STATES.LOADING,
        }, () => {
            axios.get(url)
                .then((response) => {
                    this.setState({
                        status: this.API_STATES.LOADED,
                        product: response.data,
                    })

                })
                .catch((error) => {
                    this.setState({
                        status: this.API_STATES.ERROR,
                        errorMessage: "An API error occurred. Please try again in a few minutes."
                    })
                })
        })
    }

    componentDidMount = () => {
        this.fetchData(`https://fakestoreapi.com/products/${this.props.product.productId}`)
    }

    render() {

        const { product, errorMessage, status } = { ...this.state }

        return (
            <>
                {status === this.API_STATES.LOADING && <Loader />}

                {status === this.API_STATES.ERROR &&
                    <div className='error'>
                        <h1>{errorMessage}</h1>
                    </div>
                }

                {status === this.API_STATES.LOADED && Object.keys(product).length === 0 &&
                    <div className='error'>
                        <p>"No product found</p>
                    </div>
                }

                {status === this.API_STATES.LOADED && Object.keys(product).length > 0 &&
                    <div className="container mt-1 mb-1">
                        <div className="row d-flex justify-content-center">
                            <div className="col-md-10">
                                <div className="card">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <div className="images p-2">
                                                <div className="text-center p-2">
                                                    <img
                                                        id="main-image"
                                                        src={product.image}
                                                        alt=""
                                                        width="50" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="product p-2 h-100">
                                                <div className="mt-4 mb-3">
                                                    <p className="text-uppercase text-muted mb-2">{product.category}</p>
                                                    <h6 className="text-uppercase">{product.title}</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-auto m-auto">
                                            <div className="p-2 h-100">
                                                <div className="mt-3 mb-3">
                                                    <p className="mb-2">Each price: ${product.price}</p>
                                                    <p className="mt-2">Quantity: {this.props.product.quantity}</p>
                                                    <button className="btn btn-danger"><i className="fa-solid fa-trash-can" />Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </>
        );
    }
}

export default CartProducts;
