import React, { Component } from 'react';
import '../assets/style/product.css';

class Product extends Component {

    render() {
        const productDetails = this.props.product;

        return (
            <div className="product card h-100">
                <img
                    src={productDetails.image}
                    className="product-image card-img-top img-thumbnail"
                    alt="product"
                />
                <div className="card-body text-center">
                    <h6 className="card-title">{productDetails.title}</h6>
                    <p className="card-text">{productDetails.category}</p>
                    <p className="card-text">{productDetails.rating.rate} ({productDetails.rating.count})</p>
                    <p className="card-text price">${productDetails.price}</p>
                </div>
            </div>
        );
    }
}

export default Product;