import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

import Loader from './Loader';
import '../assets/style/productDetails.css';


class ProductDetails extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        };

        this.state = {
            product: {},
            status: this.API_STATES.LOADING,
            errorMessage: "",
        };
    }

    fetchData = (url) => {
        this.setState({
            status: this.API_STATES.LOADING,
        }, () => {
            axios.get(url)
                .then((response) => {
                    this.setState({
                        status: this.API_STATES.LOADED,
                        product: response.data,
                    })

                })
                .catch((error) => {
                    this.setState({
                        status: this.API_STATES.ERROR,
                        errorMessage: "An API error occurred. Please try again in a few minutes."
                    })
                })
        })
    }

    componentDidMount = () => {
        const { match: { params } } = this.props;

        this.fetchData(`https://fakestoreapi.com/products/${params.id}`)
    }

    render() {

        const { product, errorMessage, status } = { ...this.state }

        return (
            <>
                {status === this.API_STATES.LOADING && <Loader />}

                {status === this.API_STATES.ERROR &&
                    <div className='error'>
                        <h1>{errorMessage}</h1>
                    </div>
                }

                {status === this.API_STATES.LOADED && Object.keys(product).length === 0 &&
                    <div className='error'>
                        <p>"No product found</p>
                    </div>
                }

                {status === this.API_STATES.LOADED && Object.keys(product).length > 0 &&
                    <div className="container mt-5 mb-5">
                        <div className="row d-flex justify-content-center">
                            <div className="col-md-10">
                                <div className="card">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="images p-3">
                                                <div className="text-center p-4">
                                                    <img
                                                        id="main-image"
                                                        src={product.image}
                                                        alt=""
                                                        width="250" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="product p-4 h-100">
                                                <div className="mt-4 mb-3">
                                                    <p className="text-uppercase text-muted mb-2">{product.category}</p>
                                                    <h5 className="text-uppercase mb-4">{product.title}</h5>
                                                    <h6 className="act-price mb-2">${product.price}</h6>
                                                    <p className="mt-2">{product.rating.rate} ({product.rating.count})</p>
                                                </div>
                                                <p className="about">{product.description}</p>
                                                <div className='product-button d-flex'>
                                                    <Link className="nav-link" to="/">
                                                        <button className='btn btn-primary'>Back</button>
                                                    </Link>
                                                    <button className="btn btn-danger text-uppercase mr-2 px-4">Add to cart</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </>
        );
    }
}

export default ProductDetails;