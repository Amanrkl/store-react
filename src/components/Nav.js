import React, { Component } from 'react';
import { Link } from "react-router-dom";

import '../assets/style/nav.css';

class Nav extends Component {
    render() {
        return (
            <nav className="navbar navbar-dark bg-dark mb-3">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/">
                        <i className="fa-solid fa-shop me-2"></i>
                        Fake Store
                    </Link>
                    <ul className="nav">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/products">
                                Products
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/cart/1">
                                Cart
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Nav;