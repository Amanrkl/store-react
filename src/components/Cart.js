import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import Loader from './Loader';
import CartProducts from './CartProducts';

class Cart extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        };

        this.state = {
            cart: {},
            status: this.API_STATES.LOADING,
            errorMessage: "",
        };
    }

    fetchData = (url) => {
        this.setState({
            status: this.API_STATES.LOADING,
        }, () => {
            axios.get(url)
                .then((response) => {
                    this.setState({
                        status: this.API_STATES.LOADED,
                        cart: response.data,
                    })

                })
                .catch((error) => {
                    this.setState({
                        status: this.API_STATES.ERROR,
                        errorMessage: "An API error occurred. Please try again in a few minutes."
                    })
                })
        })
    }

    componentDidMount = () => {
        const { match: { params } } = this.props;

        this.fetchData(`https://fakestoreapi.com/carts/${params.userId}`)
    }

    render() {
        const { cart, errorMessage, status } = { ...this.state };

        return (
            <>
                {status === this.API_STATES.LOADING && <Loader />}

                {status === this.API_STATES.ERROR &&
                    <div className='error'>
                        <h1>{errorMessage}</h1>
                    </div>
                }

                {status === this.API_STATES.LOADED && Object.keys(cart).length === 0 &&
                    <div className='error'>
                        <p>
                            "Currently your cart is empty"
                        </p>
                        <Link to='/'>
                            <button className='btn btn-primary'>
                                Shop Now
                            </button>
                        </Link>
                    </div>
                }

                {status === this.API_STATES.LOADED && Object.keys(cart).length > 0 &&
                    <div className='container'>
                        <h3>Your cart</h3>
                        {cart.products
                            .map(product => {
                                return (
                                    <div className='col' key={product.productId} >
                                        <CartProducts
                                            product={product}
                                        />
                                    </div>
                                )
                            })
                        }
                    </div>
                }
            </>
        );
    }
}

export default Cart;

