import React, { Component } from 'react';
import { Link } from "react-router-dom";

import Product from './Product';
import '../assets/style/products.css';

class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
        };
    }

    render() {

        const { products } = { ...this.props };

        return (
            <>
                {products.length === 0 &&
                    <div className='error'>
                        <h1>No products available at the moment. Please try again later.</h1>
                    </div>
                }

                {products.length > 0 &&
                    <div className='container'>
                        <div className='Products row row-cols-1 row-cols-md-2 row-cols-lg-4 g-4'>
                            {
                                products.map((product) => {
                                    return <div className='col' key={product.id} >
                                        <Link to={`products/${product.id}`}>
                                            <Product
                                                product={product}
                                            />
                                        </Link>
                                    </div>
                                })

                            }
                        </div>
                    </div>
                }
            </>
        );
    }
}

export default Products;