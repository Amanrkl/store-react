import React, { Component } from 'react';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Nav from './components/Nav';
import Products from './components/Products';
import ProductDetails from './components/ProductDetails';
import NoMatch from './components/NoMatch';
import Cart from './components/Cart';
import Loader from './components/Loader';

class App extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    };

    this.URL = 'https://fakestoreapi.com/products';
  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      axios.get(url)
        .then((response) => {
          this.setState({
            status: this.API_STATES.LOADED,
            products: response.data,
          })

        })
        .catch((error) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occurred. Please try again in a few minutes."
          })
        })
    })
  }

  componentDidMount = () => {
    this.fetchData(this.URL)
  }
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <Nav />
          </header>

          {this.state.status === this.API_STATES.LOADING && <Loader />}

          {this.state.status === this.API_STATES.ERROR &&
            <div className='error'>
              <h1>{this.state.errorMessage}</h1>
            </div>
          }

          {this.state.status === this.API_STATES.LOADED &&
            <Switch>
              <Route exact path="/">
                <Products
                  products={this.state.products}
                />
              </Route>

              <Route exact path="/products">
                <Products
                  products={this.state.products}
                />
              </Route>

              <Route path="/cart/:userId" render={(props) => {
                return <Cart {...props} />
              }} />

              <Route path="/products/:id" component={ProductDetails} />

              <Route path="*" component={NoMatch} />
            </Switch>
          }
        </div>
      </Router>
    );
  }
}


export default App;
